import { expect } from 'chai';
import { Meteor } from 'meteor/meteor';

import Player from '../Quidditch/models/Player';
import Chaser from '../Quidditch/models/Chaser';
import Keeper from '../Quidditch/models/Keeper';
import Seeker from '../Quidditch/models/Seeker';
import Team from '../Quidditch/models/Team';
import Game from '../Quidditch/models/Game';

const chaser1 = new Chaser({
  firstName: 'Riki',
  lastName: 'Backstab',
  number: 1
});
const chaser2 = new Chaser({
  firstName: 'Telsra',
  lastName: 'Nyx',
  number: 2
});
const chaser3 = new Chaser({
  firstName: 'Lycan',
  lastName: 'Hollow',
  number: 3
});
const seeker1 = new Seeker({
  firstName: 'Sven',
  lastName: 'Wrath',
  number: 4
});
const keeper1 = new Keeper({
  firstName: 'Drow',
  lastName: 'Sonic',
  number: 5
});
const team1 = new Team({
  name: 'Sentinel',
  players: []
});
const chaser4 = new Chaser({
  firstName: 'Drakan',
  lastName: 'Kiri',
  number: 6
});
const chaser5 = new Chaser({
  firstName: 'Enigma',
  lastName: 'Despair',
  number: 7
});
const chaser6 = new Chaser({
  firstName: 'Fruity',
  lastName: 'Apple',
  number: 8
});
const seeker2 = new Seeker({
  firstName: 'Monk',
  lastName: 'Ava',
  number: 9
});
const keeper2 = new Keeper({
  firstName: 'Rylai',
  lastName: 'Crys',
  number: 10
});
const team2 = new Team({
  name: 'Scourge',
  players: []
});
const game = new Game();

// chaser1.save();
// chaser2.save();
// chaser3.save();
// seeker1.save();
// keeper1.save();
// chaser4.save();
// chaser5.save();
// chaser6.save();
// seeker2.save();
// keeper2.save();
// team1.players.[chaser1._id, chaser2._id, chaser3._id, seeker1._id, keeper1._id];
// team1.save();
// team2.players.[chaser4._id, chaser5._id, chaser6._id, seeker2._id, keeper2._id];
// team2.save();
// game.teamOne = team1._id;
// game.teamTwo = team2._id;
// game.save();

if (Meteor.isClient) {
  describe('Chaser', () => {
    describe('#addGoalMade', () => {
      it('should increase goal made by 1', () => {
        const goalMade = chaser1.goalMade;
        let expected = 1;
        if (goalMade !== 0) {
          expected = goalMade + 1;
        }
        chaser1.addGoalMade();
        expect(chaser1.goalMade).to.equal(expected);
      });
    });
    describe('#addGoalMissed', () => {
      it('should increase goal missed by 1', () => {
        const goalMissed = chaser1.goalMissed;
        let expected = 1;
        if (goalMissed !== 0) {
          expected = goalMissed + 1;
        }
        chaser1.addGoalMissed();
        expect(chaser1.goalMissed).to.equal(expected);
      });
    });
  });
  describe('Keeper', () => {
    describe('#addGoalBlocked', () => {
      it('should increase goal blocked by 1', () => {
        const goalBlocked = keeper1.goalBlocked;
        let expected = 1;
        if (goalBlocked !== 0) {
          expected = goalBlocked + 1;
        }
        keeper1.addGoalBlocked();
        expect(keeper1.goalBlocked).to.equal(expected);
      });
    });
  });
}
if (Meteor.isServer) {
  describe('Commentator Unit-Tests', () => {
      it('should return the game results when a seeker catches the snitch', () => {
        seeker1.hasCaughtSnitch = true;
        seeker1.save();
        seeker2.save();
        team1.players.push(seeker1._id);
        team1.save();
        team2.players.push(seeker2._id);
        team2.save();
        game.teamOne = team1._id;
        game.teamTwo = team2._id;
        game.save();
        expect(game.getResults()).to.have.any.keys('winner', 'loser', 'isDraw');
      });
      it('should return the team with the highest score as a winner', () => {
        const results = game.getResults();
        expect(results.winner.getScore() > results.loser.getScore()).to.be.true;
      });
      it('should return the team with the lowest score as defeated', () => {
        const results = game.getResults();
        expect(results.loser.getScore() < results.winner.getScore()).to.be.true;
      });
      it('should not draw when both teams dont have same score', () => {
        const results = game.getResults();
        expect(results).to.not.have.property('isDraw');
      });
  });
  describe('Team', () => {
    chaser1.save();
    chaser2.save();
    chaser3.save();
    seeker1.save();
    keeper1.save();
    chaser4.save();
    chaser5.save();
    chaser6.save();
    seeker2.save();
    keeper2.save();
    it('should return null if the team has already 3 chasers when adding a chaser', () => {
      team1.addPlayer(chaser1._id);
      team1.addPlayer(chaser2._id);
      team1.addPlayer(chaser3._id);
      expect(team1.addPlayer(chaser4._id)).to.be.null;
    });
    it('should return null if the team has already 1 keeper when adding a keeper', () => {
      team1.addPlayer(keeper1._id);
      expect(team1.addPlayer(keeper2._id)).to.be.null;
    });
    it('should return null if the team has already 1 seeker when adding a seeker', () => {
      team1.addPlayer(seeker1._id);
      expect(team1.addPlayer(seeker2._id)).to.be.null;
    });
    it('should increase the team score by 10 when the chaser of the team scores', () => {
      const expected = team1.getScore() + 10;
      chaser1.addGoalMade();
      chaser1.save();
      expect(team1.getScore()).to.equal(expected);
    });
    it('should increase the team score by 30 when the seeker of the team catches the snitch', () => {
      team2.addPlayer(seeker2._id);
      let expected = team2.getScore();
      if (!seeker2.hasCaughtSnitch) {
        expected = team2.getScore() + 30;
      }
      seeker2.hasCaughtSnitch = true;
      seeker2.save();
      expect(team2.getScore()).to.equal(expected);
    });
  });

}
