import { Route } from 'react-router';
import { ReactRouterSSR } from 'meteor/reactrouter:react-router-ssr';
import todoRoutes from 'Quidditch/client/routes';
import React from 'react';

ReactRouterSSR.Run(
  <Route>
    {todoRoutes}
  </Route>
);
