import { Meteor } from 'meteor/meteor';
// import Tasks from 'TodoApp/collections/Tasks';

Meteor.methods({
  showElement: function() {
    return {display: 'block'};
  },

  hideElement: function() {
    return {display: 'none'};
  }
});
