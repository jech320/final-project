import Player from 'Quidditch/models/Player';
import Team from 'Quidditch/models/Team';
import Game from 'Quidditch/models/Game';
import Event from 'Quidditch/models/Event';

Meteor.publish('players', () => {
  return Player.col.find({});
});
Meteor.publish('teams', () => {
  return Team.col.find({});
});
Meteor.publish('games', () => {
  return Game.col.find({});
});
Meteor.publish('events', () => {
  return Event.col.find({});
});
