import { Meteor } from 'meteor/meteor';
import Player from 'Quidditch/models/Player';
import Game from 'Quidditch/models/Game';
import Team from 'Quidditch/models/Team';
import Event from 'Quidditch/models/Event';

Game.col.allow({
  insert: (userId, doc) => {
    return false;
  },
  update: (userId, doc, fields, modifier) => {
    return isCommentator(userId) && !fields._id && !fields.timeStarted;
  },
  remove: (userId, doc) => {
    return false;
  }
});
Player.col.allow({
  insert: (userId, doc) => {
    return false;
  },
  update: (userId, doc, fields, modifier) => {
    return isCommentator(userId) && !fields._id;
  },
  remove: (userId, doc) => {
    return false;
  }
});
Team.col.allow({
  insert: (userId, doc) => {
    return false;
  },
  update: (userId, doc, fields, modifier) => {
    return isCommentator(userId) && !fields._id;
  },
  remove: (userId, doc) => {
    return false;
  }
});
Event.col.allow({
  insert: (userId, doc) => {
    return isCommentator(userId) && hasEventProperties(doc);
  },
  update: (userId, doc, fields, modifier) => {
    return false;
  },
  remove: (userId, doc) => {
    return false;
  }
});

function isCommentator(userId) {
  const user = Meteor.users.findOne(userId);
  let isAllowed = false;

  if (user) {
      isAllowed = user.profile.accountType === 'Commentator';
  }
  return isAllowed;
}

function hasEventProperties(doc) {
  let isAllowed = true;

  for (var prop in doc) {
    if (prop !== timeStamp && prop !== playerId && prop !== playerAction) {
      isAllowed = false;
    }
  }

  return isAllowed;
}
