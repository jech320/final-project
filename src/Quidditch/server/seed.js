import { Meteor } from 'meteor/meteor';
import Player from 'Quidditch/models/Player';
import Game from 'Quidditch/models/Game';
import Team from 'Quidditch/models/Team';
import Chaser from 'Quidditch/models/Chaser';
import Seeker from 'Quidditch/models/Seeker';
import Keeper from 'Quidditch/models/Keeper';

Meteor.startup(() => {
  const chaser1 = new Chaser({
    firstName: 'Riki',
    lastName: 'Backstab',
    number: 1
  });
  const chaser2 = new Chaser({
    firstName: 'Telsra',
    lastName: 'Nyx',
    number: 2
  });
  const chaser3 = new Chaser({
    firstName: 'Lycan',
    lastName: 'Hollow',
    number: 3
  });
  const seeker1 = new Seeker({
    firstName: 'Sven',
    lastName: 'Wrath',
    number: 4
  });
  const keeper1 = new Keeper({
    firstName: 'Drow',
    lastName: 'Sonic',
    number: 5
  });
  const team1 = new Team({
    name: 'Sentinel',
    players: []
  });
  const chaser4 = new Chaser({
    firstName: 'Drakan',
    lastName: 'Kiri',
    number: 6
  });
  const chaser5 = new Chaser({
    firstName: 'Enigma',
    lastName: 'Despair',
    number: 7
  });
  const chaser6 = new Chaser({
    firstName: 'Fruity',
    lastName: 'Apple',
    number: 8
  });
  const seeker2 = new Seeker({
    firstName: 'Monk',
    lastName: 'Ava',
    number: 9
  });
  const keeper2 = new Keeper({
    firstName: 'Rylai',
    lastName: 'Crys',
    number: 10
  });
  const team2 = new Team({
    name: 'Scourge',
    players: []
  });
  const game = new Game();

  if (Player.find().count() === 0) {
    chaser1.save();
    chaser2.save();
    chaser3.save();
    seeker1.save();
    keeper1.save();
    chaser4.save();
    chaser5.save();
    chaser6.save();
    seeker2.save();
    keeper2.save();
  }
  if (Team.find().count() === 0) {
    team1.players.push(chaser1._id);
    team1.players.push(chaser2._id);
    team1.players.push(chaser3._id);
    team1.players.push(seeker1._id);
    team1.players.push(keeper1._id);
    team1.save();
    team2.players.push(chaser4._id);
    team2.players.push(chaser5._id);
    team2.players.push(chaser6._id);
    team2.players.push(seeker2._id);
    team2.players.push(keeper2._id);
    team2.save();
  }
  if (Game.find().count() === 0) {
    game.teamOne = team1._id;
    game.teamTwo = team2._id;
    game.save();
  }
  if (Meteor.users.find().count() === 0) {
    Accounts.createUser({
      email: 'commentator@test.com',
      password: 'test',
      profile: {
        fullName: 'Faker Hundred',
        accountType: 'Commentator'
      }
    });
  }
});
