import { Component, PropTypes } from 'react';
import ReactMixin from 'react-mixin';
import React from 'react';

import Seeker from 'Quidditch/models/Seeker';
import Chaser from 'Quidditch/models/Chaser';
import Keeper from 'Quidditch/models/Keeper';
import Team from 'Quidditch/models/Team';
import Game from 'Quidditch/models/Game';
import Player from 'Quidditch/models/Player';

export default class PlayerTableRow extends React.Component {
  static propTypes = {
    id: PropTypes.string.isRequired,
    type: PropTypes.string.isRequired
  }

  getPlayer() {
    return Player.findOne({_id: this.props.id});
  }

  getName() {
    return this.getPlayer().firstName + ' ' + this.getPlayer().lastName;
  }

  render() {
    const hide = {
      display: 'none'
    };
    const show = {
      display: 'table-cell'
    };
    return(
      <tr>
        <td>{this.getPlayer().number}</td>
        <td>{this.getName()}</td>
        <td style={this.props.type === 'Chaser' ? show : hide}>{this.getPlayer().goalMade}</td>
        <td style={this.props.type === 'Chaser' ? show : hide}>{this.getPlayer().goalMissed}</td>
        <td style={this.props.type === 'Keeper' ? show : hide}>{this.getPlayer().goalBlocked}</td>
        <td style={this.props.type === 'Seeker' ? show : hide}>{this.getPlayer().hasCaughtSnitch ? 'Caught Snitch' : 'n/a'}</td>
      </tr>
    );
  }
}
