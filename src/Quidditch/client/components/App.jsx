import { Component } from 'react';
import ReactMixin from 'react-mixin';
import React from 'react';

import Seeker from 'Quidditch/models/Seeker';
import Chaser from 'Quidditch/models/Chaser';
import Keeper from 'Quidditch/models/Keeper';
import Team from 'Quidditch/models/Team';
import Game from 'Quidditch/models/Game';
import Player from 'Quidditch/models/Player';

import PlayerTable from './PlayerTable';
import PlayerSelectionModal from './PlayerSelectionModal';

@ReactMixin.decorate(ReactMeteorData)
export default class App extends React.Component {
  getMeteorData() {
    Meteor.subscribe('players');
    Meteor.subscribe('teams');
    Meteor.subscribe('games');
    const players = Player.find().fetch();
    const teams = Team.find().fetch();
    const game = Game.findOne();

    return {players, teams, game};
  }

  loginCommentator(event) {
    event.preventDefault();

    Meteor.loginWithPassword('commentator@test.com', 'test', (err) => {
      if (!err) {
        $('#commentatorInterface').css('display','block');
        $('#loginBtn').css('display','none');
        $('#logoutBtn').css('display','block');
      } else {
        alert('Login Failed!');
      }
    });
  }

  logoutCommentator(event) {
    event.preventDefault();

    Meteor.logout((error) => {
      if (!error) {
        $('#commentatorInterface').css('display','none');
        $('#loginBtn').css('display','block');
        $('#logoutBtn').css('display','none');
      } else {
        alert('Logout Failed');
      }
    });
  }

  getTeamPlayerIds(teamNumber) {
    const playerIdsByRole = [
    {
      role: 'Chaser',
      ids: []
    },
    {
      role: 'Keeper',
      ids: []
    },
    {
      role: 'Seeker',
      ids: []
    }];
    const team = this.getTeam(teamNumber);
    // console.log(team.players);
    team.players.forEach((data) => {
      const player = Player.findOne({_id: data});

      switch (player.getRole()) {
        case 'Chaser':
          playerIdsByRole[0].ids.push(player._id);
          break;
        case 'Keeper':
          playerIdsByRole[1].ids.push(player._id);
          break;
        case 'Seeker':
          playerIdsByRole[2].ids.push(player._id);
          break;
        default:
          break;
      }
    });

    return playerIdsByRole;
  }

  getTeam(teamNumber) {
    return Team.findOne({_id: this.data.game[teamNumber]});
  }

  addSnitch(event) {
    event.preventDefault();
    let confirmMessage = '';

    if (!this.data.game.snitchAppeared) {
      this.data.game.addSnitch();
      this.data.game.save();
      confirmMessage = 'Snitch Added';
    } else if (this.data.game.getResults().hasOwnProperty('winner') || this.data.game.getResults().hasOwnProperty('isDraw')){
      confirmMessage = 'Error! Game has ended!';
    } else {
      confirmMessage = 'Error! Snitch already exists!';
    }

    alert(confirmMessage);
  }

  getResult(teamNumber) {
    const results = this.data.game.getResults();
    const team = this.getTeam(teamNumber);
    let result = '';

    if (this.data.game.getResults().hasOwnProperty('winner')) {
      $('#teamOneStatus').css('display','block');
      $('#teamTwoStatus').css('display','block');
      if (results.winner._id === team._id) {
        result = 'Winner';
      } else if (results.loser._id === team._id) {
        result = 'Defeated';
      }
    } else if (this.data.game.getResults().hasOwnProperty('isDraw')) {
      $('#teamOneStatus').css('display','block');
      $('#teamTwoStatus').css('display','block');
      result = 'Draw';
    } else {
      $('#teamOneStatus').css('display','none');
      $('#teamTwoStatus').css('display','none');
    }

    return result;
  }

  resetGame(event) {
    event.preventDefault();

    const game = this.data.game;
    const teamOne = Team.findOne({_id: game.teamOne});
    const teamTwo = Team.findOne({_id: game.teamTwo});

    game.snitchAppeared = undefined;
    this.resetTeamMembersScore(teamOne);
    this.resetTeamMembersScore(teamTwo);
  }

  resetTeamMembersScore(team) {
    team.players.forEach((data) => {
      let player = Player.findOne({_id: data});

      switch (player.getRole()) {
        case 'Chaser':
          player = new Chaser(player);
          player.goalMade = 0;
          player.goalMissed = 0;
          break;
        case 'Keeper':
          player = new Keeper(player);
          player.goalBlocked = 0;
          break;
        case 'Seeker':
          player = new Seeker(player);
          player.hasCaughtSnitch = false;
          break;
        default:
          break;
      }

      player.save();
    });
  }

  render() {
    if (!this.data.players || !this.data.game || !this.data.teams) {
      return false;
    }
    const show = {
      display: 'block'
    };
    const hide = {
      display: 'none'
    };
    const showInline = {
      display: 'inline-block'
    };

    return (
      <div className="container text-center">
        <PlayerSelectionModal game={this.data.game} teamOne={this.getTeam('teamOne')} teamTwo={this.getTeam('teamTwo')}/>
        <div className="row">
          <div className="col-md-5 col-xs-4">
            <h1 id="teamOneStatus">{this.getResult('teamOne')}</h1>
            <h3>Team {this.getTeam('teamOne').name}</h3>
            <h5>{this.getTeam('teamOne').getScore()}</h5>
          </div>
          <div className="col-md-2 col-xs-4 text-center">
            <h1>VS</h1>
          </div>
          <div className="col-md-5 col-xs-4">
            <h1 id="teamTwoStatus">{this.getResult('teamTwo')}</h1>
            <h3>Team {this.getTeam('teamTwo').name}</h3>
            <h5>{this.getTeam('teamTwo').getScore()}</h5>
          </div>
        </div>
        <hr/>
        <div className="row wrapper" id="commentatorInterface" style={Meteor.user() ? show : hide}>
          <div className="col-sm-12">
            <button className="btn addGoalMadeBtn" data-toggle="modal" data-target="#playerSelectionModal" data-action="Add Goal Made">Goal Made</button>
            <button className="btn addGoalMissedBtn" data-toggle="modal" data-target="#playerSelectionModal" data-action="Add Goal Missed">Goal Missed</button>
            <button className="btn addGoalBlockedBtn" data-toggle="modal" data-target="#playerSelectionModal" data-action="Add Goal Blocked">Goal Blocked</button>
            <button className="btn addSnitchBtn" onClick={this.addSnitch.bind(this)}>Add Snitch</button>
            <button className="btn snitchCaughtBtn" data-toggle="modal" data-target="#playerSelectionModal" data-action="Set Snitch Caught">Snitch Caught</button>
          </div>
        </div>
        <div className="row">
          <div className="col-md-6 col-xs-6 right-padding">
            <div className="wrapper border-rounded">
              {this.getTeamPlayerIds('teamOne').map(data => <PlayerTable key={data.role} ids={data.ids} type={data.role}/>)}
            </div>
          </div>
          <div className="col-md-6 col-xs-6 right-padding">
            <div className="wrapper border-rounded">
              {this.getTeamPlayerIds('teamTwo').map(data => <PlayerTable key={data.role} ids={data.ids} type={data.role}/>)}
            </div>
          </div>
        </div>
        <hr/>
        <div className="row wrapper">
          <div className="col-sm-12">
            <button onClick={this.loginCommentator.bind(this)} style={Meteor.user() ? hide : showInline} className="btn" id="loginBtn">Log-in Commentator</button>
            <button onClick={this.logoutCommentator.bind(this)} style={Meteor.user() ? showInline : hide} className="btn" id="logoutBtn">Log-out Commentator</button>
            <button onClick={this.resetGame.bind(this)} className="btn">Reset</button>
          </div>
        </div>
      </div>
    );
  }
}
