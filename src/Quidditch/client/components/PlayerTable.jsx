import { Component, PropTypes } from 'react';
import ReactMixin from 'react-mixin';
import React from 'react';

import Seeker from 'Quidditch/models/Seeker';
import Chaser from 'Quidditch/models/Chaser';
import Keeper from 'Quidditch/models/Keeper';
import Team from 'Quidditch/models/Team';
import Game from 'Quidditch/models/Game';
import Player from 'Quidditch/models/Player';

import PlayerTableRow from './PlayerTableRow';

export default class PlayerTable extends React.Component {
  static propTypes = {
    ids: PropTypes.array.isRequired,
    type: PropTypes.string.isRequired
  }

  render() {
    const hide = {
      display: 'none'
    };
    const show = {
      display: 'table-header-group'
    };
    return(
      <div className="container-fluid">
        <div className="row">
          <div className="col-sm-12">
            <div className="panel panel-default">
              <div className="panel-heading text-left" id="table-heading">
                <strong>{this.props.type}</strong>
              </div>
              <table className="table" id="testTable">
                <thead style={this.props.type === 'Chaser' ? show : hide}>
                  <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>Goal Made</th>
                    <th>Goal Missed</th>
                  </tr>
                </thead>
                <thead style={this.props.type === 'Keeper' ? show : hide}>
                  <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>Goal Blocked</th>
                  </tr>
                </thead>
                <thead style={this.props.type === 'Seeker' ? show : hide}>
                  <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>Snitch Caught</th>
                  </tr>
                </thead>
                <tbody>
                  {this.props.ids.map(data => <PlayerTableRow key={data} id={data} type={this.props.type}/>)}
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
