import { Component, PropTypes } from 'react';
import ReactMixin from 'react-mixin';
import React from 'react';

import Seeker from 'Quidditch/models/Seeker';
import Chaser from 'Quidditch/models/Chaser';
import Keeper from 'Quidditch/models/Keeper';
import Team from 'Quidditch/models/Team';
import Game from 'Quidditch/models/Game';
import Player from 'Quidditch/models/Player';

export default class PlayerSelectionModal extends React.Component {
  static propTypes = {
    teamOne: PropTypes.object.isRequired,
    teamTwo: PropTypes.object.isRequired,
    game: PropTypes.object.isRequired
  }

  componentDidMount() {
    $('#playerSelectionModal').on('show.bs.modal', (event) => {
      const button = $(event.relatedTarget);
      const action = button.data('action');
      const modal = $(this);
      document.getElementById('modal-title').innerHTML = action;
      this.action = action;
    });
  }

  checkTeamSelected(event) {
    event.preventDefault();

    if ($('#teamSelected').val() === 'SelectTeam') {
      $('#playerSelected').css('display', 'none');
    } else {
      $('#playerSelected').css('display', 'block');
      const action = this.action;
      const teamSelected = this.getTeamSelected();
      let playerRole = '';

      if (action === 'Add Goal Made' || action === 'Add Goal Missed') {
        playerRole = 'Chaser';
      } else if (action === 'Add Goal Blocked') {
        playerRole = 'Keeper';
      } else if (action === 'Set Snitch Caught' || action === 'Add Snitch') {
        playerRole = 'Seeker';
      }

      let players = teamSelected.players.map((data) => {
        const player = Player.findOne({_id: data});

        return player.getRole() === playerRole ? player : null;
      });

      players = players.filter((data) => {
        return data !== null;
      });

      this.removeOptions();

      players.forEach((data) => {
        this.addOption(data);
      });
    }
  }

  getTeamSelected() {
    return document.getElementById('teamSelected').value === 'TeamOne' ? this.props.teamOne : this.props.teamTwo;
  }

  addOption(obj) {
    const option = document.createElement('option');
    option.text = '(' + obj.number + ') ' + obj.firstName + ' ' + obj.lastName;
    option.value = obj._id;
    document.getElementById('playerSelected').add(option);
  }

  removeOptions() {
    const selectElement = document.getElementById('playerSelected');

    if (selectElement) {
      for (var i = selectElement.length - 1; i > 0; i--) {
        selectElement.remove(i);
      }
    }
  }

  resetModalForm(event) {
    event.preventDefault();

    $('#teamSelected').val('SelectTeam');
    $('#playerSelected').val('SelectPlayer');
    $('#playerSelected').css('display','none');
    this.removeOptions();
  }

  saveAction(event) {
    event.preventDefault();

    let confirmMessage = '';
    const teamSelected = this.getTeamSelected();
    const playerSelected = $('#playerSelected').val();
    const action = this.action;

    if (teamSelected !== 'SelectTeam' && playerSelected !== 'SelectPlayer' && !this.props.game.getResults().hasOwnProperty('winner')
            && !this.props.game.getResults().hasOwnProperty('isDraw')) {
      let player = Player.findOne({_id: playerSelected});

      if (action === 'Add Goal Made') {
        player = new Chaser(player);
        player.addGoalMade();
        confirmMessage = 'Successfully Added Goal Made to Player';
      } else if (action === 'Add Goal Missed') {
        player = new Chaser(player);
        player.addGoalMissed();
        confirmMessage = 'Successfully Added Missed Made to Player';
      } else if (action === 'Add Goal Blocked') {
        player = new Keeper(player);
        player.addGoalBlocked();
        confirmMessage = 'Successfully Added Blocked Made to Player';
      } else if (action === 'Set Snitch Caught' && this.props.game.snitchAppeared ) {
        player = new Seeker(player);
        player.hasCaughtSnitch = true;
        confirmMessage = 'Successfully Set Caught Snitch to Player';
      } else if (!this.props.game.snitchAppeared){
        confirmMessage = 'Error! Snitch dont exist!';
      }

      player.save();
    } else if (teamSelected == 'SelectTeam' || playerSelected == 'SelectPlayer') {
      confirmMessage = 'Error! None Selected!';
    } else {
      confirmMessage = 'Error! Game has ended!';
    }

    $('#playerSelectionModal').modal('hide');
    this.resetModalForm(event);
    alert(confirmMessage);
  }

  render() {
    const hide = {
      display: 'none'
    };
    const show = {
      display: 'block'
    };
    return(
      <div>
        <div className="modal playerSelectionModal" id="playerSelectionModal" role="dialog">
            <div className="modal-dialog">
              <div className="modal-content">
                <div className="modal-header">
                  <button type="button" className="close" data-dismiss="modal">&times;</button>
                  <h4 className="modal-title text-left" id="modal-title"></h4>
                </div>
                <div className="modal-body">
                  <div className="form-group">
                      <select name="teamSelected" id="teamSelected" className="form-control" onChange={this.checkTeamSelected.bind(this)} defaultValue="SelectTeam">
                        <option value="SelectTeam">Select Team</option>
                        <option value="TeamOne">{this.props.teamOne.name}</option>
                        <option value="TeamTwo">{this.props.teamTwo.name}</option>
                      </select>
                  </div>
                  <div className="form-group">
                      <select name="playerSelected" id="playerSelected" className="form-control" defaultValue="SelectPlayer" style={hide}>
                        <option value="SelectPlayer">Select Player</option>
                      </select>
                  </div>
                </div>
                <div className="modal-footer">
                  <button type="button" className="btn btn-primary" onClick={this.saveAction.bind(this)}>
                    Save
                  </button>
                  <button type="button" className="btn btn-default" data-dismiss="modal" onClick={this.resetModalForm.bind(this)}>
                    Cancel
                  </button>
                </div>
              </div>
            </div>
        </div>
      </div>
    );
  }
}
