import { Route } from 'react-router';
import App from './components/App';
import React from 'react';

export default (
  <Route path="/" component={App} />
);
