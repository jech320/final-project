import Model from './Model';

export default class Event extends Model {
  constructor(doc = {}) {
		super(doc);
    this.timeStamp = !this.timeStamp ? new Date() : this.timeStamp;
	}
}
