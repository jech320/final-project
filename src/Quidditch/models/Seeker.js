import Model from './Model';
import Player from './Player';

export default class Seeker extends Player {
  constructor(doc = {}) {
		super(doc);
    this.role = 'Seeker';

    if (!doc.hasOwnProperty('hasCaughtSnitch')) {
      this.hasCaughtSnitch = false;
    }
	}
}
