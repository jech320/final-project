export default class Model {
	constructor(doc = {}) {
		_(this).extend(doc);
	}

	save() {
		let model = this.constructor.col;
		let id = this._id;

		if(id == null) {
			this._id = model.insert(this.doc, function(error, result) {
				if (error) {
					console.log(error);
				} else {
					console.log(result);
				}
			});
		} else {
			model.update(id, {$set:this.doc}, function(error, result) {
				if (error) {
					console.log(error);
				} else {
					console.log(result);
				}
			});
		}
	}

	get doc() {
		const notNeeded = ['_id'];

		for(let key in this) {
			if (_(this[key]).isFunction()) {
				notNeeded.push(key);
			}

			return _(this).omit(notNeeded);
		}
	}

	static find(selector = {}, options = {}) {
    return this.col.find(selector, options);
  }

  static findOne(selector = {}, options = {}) {
    return this.col.findOne(selector, options);
  }

  static insert(doc, callback) {
    return this.col.insert(doc, callback);
  }

  static update(selector, modifier, options, callback) {
    return this.col.update(selector, modifier, options, callback);
  }

  static remove(selector = {}, callback) {
    return this.col.remove(selector, callback);
  }
}
