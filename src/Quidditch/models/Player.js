import Model from './Model';
import Team from './Team';
import Game from './Game';

export default class Player extends Model {
  static col = new Mongo.Collection('players', {
		transform: (doc) => {
			return new Player(doc);
		}
	});

  getRole() {
    return this.role;
  }

  getWinRate() {
    const playerId = this._id;
    const teamsJoined = Team.find({players: playerId}).fetch();
    let wins = 0;
    let loses = 0;

    teamsJoined.forEach((data) => {
      const games = Game.find({teamOne: data._id,teamTwo: data._id}).fetch();

      games.forEach((game) => {
        if (game.getResults().winner === data._id) {
          wins += 1;
        } else {
          loses += 1;
        }
      });
    });

    return wins / (wins + loses);
  }
}
