import Model from './Model';
import Player from './Player';
import Team from './Team';
import Event from './Event';

export default class Game extends Model {
  static col = new Mongo.Collection('games', {
		transform: (doc) => {
			return new Game(doc);
		}
	});

  constructor(doc = {}) {
		super(doc);
    this.timeStarted = !this.timeStarted ? new Date() : this.timeStarted;
	}

  addEvent(event) {
    this.events.push(event);
  }

  getResults() {
    const teamOne = Team.findOne({_id: this.teamOne});
    const teamTwo = Team.findOne({_id: this.teamTwo});
    const playerIds = teamOne.players.concat(teamTwo.players);
    let results = {};

    playerIds.forEach((data) => {
      const player = Player.findOne({_id: data});

      if (player.getRole() === 'Seeker' && player.hasCaughtSnitch) {
        this.timeEnded = new Date();

        if (teamOne.getScore() === teamTwo.getScore()) {
          results.isDraw = true;
        } else if (teamOne.getScore() > teamTwo.getScore()) {
          results.winner = teamOne;
          results.loser = teamTwo;
        } else if (teamOne.getScore() < teamTwo.getScore()){
          results.winner = teamTwo;
          results.loser = teamOne;
        }
      }
    });

    return results;
  }

  addSnitch() {
    this.snitchAppeared = !this.snitchAppeared ? new Date() : this.snitchAppeared;
  }
}
