import Model from './Model';
import Player from './Player';

export default class Team extends Model {
  static col = new Mongo.Collection('teams', {
		transform: (doc) => {
			return new Team(doc);
		}
	});

  addPlayer(id) {
    let seekerCount = 0;
    let keeperCount = 0;
    let chaserCount = 0;

    this.players.forEach((data) => {
      switch(Player.findOne({_id: data}).getRole()) {
        case 'Seeker':
          seekerCount += 1;
          break;
        case 'Keeper':
          keeperCount += 1;
          break;
        case 'Chaser':
          chaserCount += 1;
          break;
        default:
          break;
      }
    });

    const playerRole = Player.findOne({_id: id}).getRole();

    if (playerRole === 'Seeker' && seekerCount === 1) {
      return null;
    } else if (playerRole === 'Keeper' && keeperCount === 1) {
      return null;
    } else if (playerRole === 'Chaser' && chaserCount === 3) {
      return null;
    } else {
      this.players.push(id);
    }
  }

  getScore() {
    const players = this.players;
    let score = 0;

    players.forEach((data) => {
      const player = Player.findOne({_id: data});

      if (player.getRole() === 'Seeker') {
        score += player.hasCaughtSnitch ? 30 : 0;
      } else if (player.getRole() === 'Chaser') {
        score += player.goalMade * 10;
      }
    });

    return score;
  }
}
