import Model from './Model';
import Player from './Player';

export default class Chaser extends Player {
  constructor(doc = {}) {
		super(doc);
    this.role = 'Chaser';
    
    if (!doc.hasOwnProperty('goalMade') && !doc.hasOwnProperty('goalMissed')) {
      this.goalMade = 0;
      this.goalMissed = 0;
    }
	}

  addGoalMade() {
    this.goalMade += 1;
  }

  addGoalMissed() {
    this.goalMissed += 1;
  }
}
