import Model from './Model';
import Player from './Player';

export default class Keeper extends Player {
  constructor(doc = {}) {
		super(doc);
    this.role = 'Keeper';

    if (!doc.hasOwnProperty('goalBlocked')) {
      this.goalBlocked = 0;
    }
	}

  addGoalBlocked() {
    this.goalBlocked += 1;
  }
}
